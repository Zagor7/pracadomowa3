class Square:
    def __init__(self, p):
        self.p = p

    def __eq__(self, other):
        return self.p == other.p

    def __lt__(self, other):
        return self.p < other.p

    def __gt__(self, other):
        return self.p > other.p

    def __le__(self, other):
        return self.p <= other.p

    def __ge__(self, other):
        return self.p >= other.p

    def __ne__(self, other):
        return self.p != other.p


square1 = Square(40)
square2 = Square(40)

print("Rowny: ", square1 == square2)
print("Mniejszy: ", square1 < square2)
print("Wiekszy: ", square1 > square2)
print("Wiekszy rowny: ", square1 >= square2)
print("Mniejszy rowny: ", square1 <= square2)
print("Rozny: ", square1 != square2)
