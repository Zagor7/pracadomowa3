class Cuboid:
    def __init__(self, v):
        self.v = v

    def __eq__(self, other):
        return self.v == other.v

    def __lt__(self, other):
        return self.v < other.v

    def __gt__(self, other):
        return self.v > other.v

    def __le__(self, other):
        return self.v <= other.v

    def __ge__(self, other):
        return self.v >= other.v

    def __ne__(self, other):
        return self.v != other.v


cuboid1 = Cuboid(70)
cuboid2 = Cuboid(62)

print("Rowny: ", cuboid1 == cuboid2)
print("Mniejszy: ", cuboid1 < cuboid2)
print("Wiekszy: ", cuboid1 > cuboid2)
print("Wiekszy rowny: ", cuboid1 >= cuboid2)
print("Mniejszy rowny: ", cuboid1 <= cuboid2)
print("Rozny: ", cuboid1 != cuboid2)
